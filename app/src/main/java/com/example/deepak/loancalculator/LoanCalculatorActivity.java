package com.example.deepak.loancalculator;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.text.Editable; //Interface allowing you to change the content and markup of text in a GUI.
import android.text.TextWatcher; //respond to events when the user interacts with an EditText component
import android.widget.EditText; //widget and layout for EditText component


/**
 * This class renders the layout for Loan Calculator and do the required computation
 * of calculating EMI and Repayment on values given by user.
 */

public class LoanCalculatorActivity extends AppCompatActivity {

    //constants used when saving/restoring state
    private final String AMOUNT = "AMOUNT";
    private final String RATE = "RATE";

    // variable to store the principle amount provided be user.
    private double amount;
    // variable to store the rate of interest provided by user.
    private double rate;

    // variables to fetch values given by users on edit texts.
    private EditText amountEditText;
    private EditText rateEditText;

    private EditText fiveYearsEmiEditText;
    private EditText tenYearsEmiEditText;
    private EditText fifteenYearsEmiEditText;
    private EditText twentyYearsEmiEditText;
    private EditText twentyFiveYearsEmiEditText;
    private EditText thirtyYearsEmiEditText;

    private EditText fiveYearsRepaymentEditText;
    private EditText tenYearsRepaymentEditText;
    private EditText fifteenYearsRepaymentEditText;
    private EditText twentyYearsRepaymentEditText;
    private EditText twentyFiveYearsRepaymentEditText;
    private EditText thirtyYearsRepaymentEditText;

    /**
     * This method get executed on start of application.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // it renders the layout.
        setContentView(R.layout.activity_loan_calculator);
        if (savedInstanceState == null) {
            amount = 0.0;
            rate = 0.0;
        } else {
            amount = savedInstanceState.getDouble(AMOUNT);
            rate = savedInstanceState.getDouble(RATE);
        }

        // fetching values from gui, given by user in space of amount editText and binding this
        // variable with addTextChangedListener -> amountEditTextWatcher
        amountEditText = (EditText) findViewById(R.id.amountEditText);
        amountEditText.addTextChangedListener(amountEditTextWatcher);

        // fetching values from gui, given by user in space of rate editText and binding this
        // variable with addTextChangedListener -> rateEditTextWatcher
        rateEditText = (EditText) findViewById(R.id.rateEditText);
        rateEditText.addTextChangedListener(rateEditTextWatcher);

        // binding variables to emi EditTexts so that we can display results.
        fiveYearsEmiEditText = (EditText) findViewById(R.id.fiveYearsEmiEditText);
        tenYearsEmiEditText = (EditText) findViewById(R.id.tenYearsEmiEditText);
        fifteenYearsEmiEditText = (EditText) findViewById(R.id.fifteenYearsEmiEditText);
        twentyYearsEmiEditText = (EditText) findViewById(R.id.twentyYearsEmiEditText);
        twentyFiveYearsEmiEditText = (EditText) findViewById(R.id.twentyFiveYearsEmiEditText);
        thirtyYearsEmiEditText = (EditText) findViewById(R.id.thirtyYearsEmiEditText);

        // binding variables to repayment EditTexts so that we can display results.
        fiveYearsRepaymentEditText = (EditText) findViewById(R.id.fiveYearsRepaymentEditText);
        tenYearsRepaymentEditText = (EditText) findViewById(R.id.tenYearsRepaymentEditText);
        fifteenYearsRepaymentEditText = (EditText) findViewById(R.id.fifteenYearsRepaymentEditText);
        twentyYearsRepaymentEditText = (EditText) findViewById(R.id.twentyYearsRepaymentEditText);
        twentyFiveYearsRepaymentEditText = (EditText) findViewById(R.id.twentyFiveYearsRepaymentEditText);
        thirtyYearsRepaymentEditText = (EditText) findViewById(R.id.thirtyYearsRepaymentEditText);
    }

    /**
     * This method store the state of variables: amount(principle) and rate(interest).
     *
     * @param outState
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putDouble(AMOUNT, amount);
        outState.putDouble(RATE, rate);
    }

    /**
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_loan_calculator, menu);
        return true;
    }

    /**
     * This is TextWatcher which is binded with amount editText.
     */
    private TextWatcher amountEditTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            try {
                amount = Double.parseDouble(s.toString());
            } catch (NumberFormatException e) {
                amount = 0.0;
            }
            updateEMI();
            updateRepaymentAmount();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    /**
     * This is TextWatcher which is binded with rate editText
     */
    private TextWatcher rateEditTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            try {
                rate = Double.parseDouble(s.toString());
            } catch (NumberFormatException e) {
                rate = 0.0;
            }
            updateEMI();
            updateRepaymentAmount();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    /**
     * This method updates the emi editTexts values for all years
     */
    private void updateEMI() {
        fiveYearsEmiEditText.setText(String.format("%.02f", calculateEMI(amount, rate, 5)));
        tenYearsEmiEditText.setText(String.format("%.02f", calculateEMI(amount, rate, 10)));
        fifteenYearsEmiEditText.setText(String.format("%.02f", calculateEMI(amount, rate, 15)));
        twentyYearsEmiEditText.setText(String.format("%.02f", calculateEMI(amount, rate, 20)));
        twentyFiveYearsEmiEditText.setText(String.format("%.02f", calculateEMI(amount, rate, 25)));
        thirtyYearsEmiEditText.setText(String.format("%.02f", calculateEMI(amount, rate, 30)));
    }

    /**
     * This method updates the repayment editTexts values for all years
     */
    private void updateRepaymentAmount() {
        fiveYearsRepaymentEditText.setText(String.format("%.02f",
                calculateRepaymentAmount(amount, rate, 5)));
        tenYearsRepaymentEditText.setText(String.format("%.02f",
                calculateRepaymentAmount(amount, rate, 10)));
        fifteenYearsRepaymentEditText.setText(String.format("%.02f",
                calculateRepaymentAmount(amount, rate, 15)));
        twentyYearsRepaymentEditText.setText(String.format("%.02f",
                calculateRepaymentAmount(amount, rate, 20)));
        twentyFiveYearsRepaymentEditText.setText(String.format("%.02f",
                calculateRepaymentAmount(amount, rate, 25)));
        thirtyYearsRepaymentEditText.setText(String.format("%.02f",
                calculateRepaymentAmount(amount, rate, 30)));
    }

    /**
     * This method calculates the emi.
     *
     * @param amount
     * @param rate
     * @param years
     * @return emi
     */
    private double calculateEMI(double amount, double rate, int years) {
        // to omit NaN cases
        if (rate == 0) {
            return 0.0;
        }
        double emi = ((amount * (rate / 1200) * Math.pow((1 + (rate / 1200)), years * 12)) /
                (Math.pow((1 + (rate / 1200)), years * 12) - 1));
        return emi;
    }

    /**
     * This method calculates the total Repayment.
     *
     * @param amount
     * @param rate
     * @param years
     * @return double.
     */
    private double calculateRepaymentAmount(double amount, double rate, int years) {
        // to omit NaN cases
        if (rate == 0) {
            return 0.0;
        }
        return (calculateEMI(amount, rate, years) * years * 12);
    }
}
